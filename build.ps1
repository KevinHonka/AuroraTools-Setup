﻿$version = "v0.5.0-beta"

# Activate venv
.\venv\Scripts\Activate.ps1

# Download zip file
wget https://gitlab.com/KevinHonka/aurora_tools/-/archive/$version/aurora_tools-$version.zip -OutFile aurora_tools-$version.zip

# Expand zipfile
Expand-Archive aurora_tools-$version.zip -DestinationPath aurora_tools

# go to subdirectory
cd aurora_tools\aurora_tools-$version

# install requirements of AuroraTools
pip install -r requirements.txt

# Build Exe
pyinstaller --name="AuroraTools" --windowed --hidden-import PySide6.QtXml tools.py

# Supply needed libs
cp -r ..\..\venv\Lib\site-packages\PySide6\plugins\platforms .\dist\AuroraTools\
cp -r ..\..\venv\Lib\site-packages\qt_material .\dist\AuroraTools\
cp -r .\icons .\dist\AuroraTools\
cp -r .\templates .\dist\AuroraTools\
cp -r .\export_templates .\dist\AuroraTools\

# Package as zip
Add-Type -assembly "system.io.compression.filesystem"
[io.compression.zipfile]::CreateFromDirectory($PWD.Path + "\dist\AuroraTools", $PWD.Path + "\AuroraTools-$version.zip")