## Building app

### Create venv
Powershell:

``python -m venv venv``

``.\venv\Scripts\Activate.ps1``

``pip install -r requirements.txt``

### Download version
Powershell:

``wget https://gitlab.com/KevinHonka/aurora_tools/-/archive/0.3.0/aurora_tools-0.3.0.zip -OutFile aurora_tools.zip``


### Unpack
Powershell:

``Expand-Archive aurora_tools.zip -DestinationPath aurora_tools``

``cd aurora_tools\\aurora_tools-0.3.0``

``pip install -r requirements.txt``


### Build

``pyinstaller --name="AuroraTools" --hidden-import PySide6.QtXml tools.py``

``cp -r ..\..\venv\Lib\site-packages\PySide6\plugins\platforms .\dist\AuroraTools\``

``cp -r ..\..\venv\Lib\site-packages\qt-material .\dist\AuroraTools\``

``cp -r .\icons .\dist\AuroraTools\``
